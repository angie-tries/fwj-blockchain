const Blockchain = require('./lib/blockchain')
const newChain = new Blockchain()

// const myTranx = [300, -150, 100, -200]

const myTranx = [
  {
    data: null,
    hash: '0000d16c44dae2a113cc49289c3f97d84fd66f175c9ee4f8acd47a985a74722f',
    nonce: 110636,
    prevHash: null
  },
  {
    data: { user: 'angie', amount: 300 },
    hash: '0000e9e9283cc2d03c2cb475e65296ebefcdd9935b0a4fef266c5b22a75a201d',
    nonce: 38932,
    prevHash: '0000d16c44dae2a113cc49289c3f97d84fd66f175c9ee4f8acd47a985a74722f'
  },
  {
    data: { user: 'angie', amount: -150 },
    hash: '0000c643c75535dd8581631782e8dfa1399510a4669a4cc63cc7f4afaab4a316',
    nonce: 136389,
    prevHash: '0000e9e9283cc2d03c2cb475e65296ebefcdd9935b0a4fef266c5b22a75a201d'
  },
  {
    data: { user: 'angie', amount: 100 },
    hash: '0000d55da350093fcc0d0bcabfbd8c3e06a67d18f5c4e19b9ea594fcf4bf08d1',
    nonce: 48049,
    prevHash: '0000c643c75535dd8581631782e8dfa1399510a4669a4cc63cc7f4afaab4a316'
  },
  {
    data: { user: 'angie', amount: -200 },
    hash: '0000ee812f6db956ad8cbb10481c2640b578e33045c9662daf9fd26fde8e9b17',
    nonce: 23922,
    prevHash: '0000d55da350093fcc0d0bcabfbd8c3e06a67d18f5c4e19b9ea594fcf4bf08d1'
  }
]

for (let i = 0; i < myTranx.length; i++) {
  // newChain.addBlock({
  //   user: 'angie',
  //   amount: myTranx[i]
  // })
  newChain.addBlock(myTranx[i].data)
}

const validate = chain => {
  const blocks = chain.blocks

  if (blocks.length > 1) {
    for (let i = 0; i < blocks.length - 1; i++) {
      if (i + 1 === blocks.length) {
        return
      }

      const currBlock = blocks[i]
      const nextBlock = blocks[i + 1]

      if (!(nextBlock.prevHash === currBlock.hash)) {
        throw new Error('Chain not validated.\n' + currBlock)
      }
    }
  }

  return 'No new blocks'
}

validate(newChain)
newChain.blocks.map(b => console.log(b))
