const crypto = require('crypto')

class Block {
  constructor (prevHash, data) { // class properties, in es7 can skip the contructor fn
    // this.hash = Date.now().toString() + JSON.stringify(data) + Math.random().toString().replace('.', '')
    this.data = data
    this.hash = this.findHash(data).answer
    this.nonce = this.findHash(data).nonce
    this.prevHash = prevHash
  }

  findHash (data) {
    let nonce = 0
    while (true) {
      // this is a one way function
      const hash = crypto.createHash('sha256')
      const answer = hash.update(JSON.stringify(data) + nonce).digest('hex')

      if (answer.startsWith('0000')) {
        return {answer, nonce}
      }
      nonce++
    }
  }
}

module.exports = Block
