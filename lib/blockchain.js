const Block = require('./block')

class Blockchain {
  constructor () {
    this.blocks = [ new Block(null, null) ]
    this.currentTransaction = null
    // this.difficulty = 0
  }

  addBlock (data) {
    const newBlock = new Block(this.lastBlock().hash, data)
    this.blocks.push(newBlock)
  }

  lastBlock () {
    return this.blocks[this.blocks.length - 1]
  }

  // difficulty () {
  //   if (this.blocks.length > ) {

  //   }
  // }
}

module.exports = Blockchain
